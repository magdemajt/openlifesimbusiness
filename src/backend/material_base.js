import Material from './Material';

export const raws = {
  grain: new Material('grain', 0.1, 1),
  iron: new Material('iron', 10, 0, 3),
  copper: new Material('copper', 30, 0, 5),
  aluminium: new Material('aluminium', 50, 0, 5),
  silicon: new Material('silicon', 100, 0, 10),
  coal: new Material('coal', 5, 0, 5),
  oil: new Material('oil', 6, 0, 7),
  gold: new Material('gold', 1000, 0, 1),
};

export const processed = {
  graphene: new Material('graphene', 100000, 1, 20, [{ material: raws.coal, amount: 10 }]),
  car_parts: new Material('car parts', 10000, 1, 5, [{ material: raws.iron, amount: 10 }, { material: raws.aluminium, amount: 20 }, { material: raws.copper, amount: 2 },]),
  computer_parts: new Material('computer parts', 200, 1, 30, [{ material: raws.silicon, amount: 5 }, { material: raws.aluminium, amount: 8 }, { material: raws.copper, amount: 10 },]),
  quantum_computer_parts: new Material('quantum computer parts', 20000, 1, 30, [{ material: raws.silicon, amount: 5 }, { material: raws.aluminium, amount: 8 }, { material: raws.copper, amount: 10 }, { material: raws.gold, amount: 10 }, ]),
  food: new Material('food', 1, 1, 1, [{ material: raws.grain, amount: 50 }]),
}

export const highly_processed = {
  cars: new Material('cars', 50000, 2, 30, [{ material: processed.car_parts, amount: 10 }, { material: raws.aluminium, amount: 20 }, { material: processed.computer_parts, amount: 2 },]),
  computers: new Material('computers', 1000, 2, 15, [{ material: processed.computer_parts, amount: 5 }, { material: raws.silicon, amount: 1 }]),
  electronics: new Material('electronics', 200, 2, 10, [{ material: processed.computer_parts, amount: 1 }, { material: raws.silicon, amount: 2 }, { material: raws.copper, amount: 2 }]),
}

export const services = {
  
}

const materials = [];