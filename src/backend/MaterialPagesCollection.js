import { useState, Component } from 'react';
export default class MaterialPagesCollection extends Component {
  constructor () {
    this.pages = [];
    this.materialPages = [];
    this.state = {};
    const [displayState, setDisplayState] = useState(this.state);
    this.displayState = displayState;
    this.setDisplayState = setDisplayState;
  }
  updateState = () => {
    this.setDisplayState(this.state);
  }
}