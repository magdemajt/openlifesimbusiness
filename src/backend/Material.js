import { random } from 'lodash';
export default class Material {
  constructor (name, startingCost, processedLevel, breakthroughChance = 0, neededMaterials = []) {
    this.name = name;
    this.startingCost = startingCost;
    this.processedLevel = processedLevel; //0 - raw (iron), 1 - processed (car parts) 2 - highly processed (computer) 3 - services (special treatment)
    this.breakthroughChance = breakthroughChance;
    this.cost = startingCost;
    this.breakthroughLevel = 0;
    this.neededMaterials = neededMaterials; // {material: amount}
  }
  breakthrough = () => {
    if (random(0, 100) < this.breakthroughChance) {
      this.breakthroughLevel += 1;
      this.cost = this.cost / 2;
    }
  }
}