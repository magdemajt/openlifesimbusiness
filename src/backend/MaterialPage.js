export default class MaterialPage {
  constructor () {
    this.material = '';
    this.icon = { text: '', icon: '' }
    this.buttons = []; // { name, text, onClick, position }
    this.texts = []; // { name, value, position }
  }
}